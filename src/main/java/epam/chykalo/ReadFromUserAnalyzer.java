package epam.chykalo;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ReadFromUserAnalyzer {
    List<String> list = new ArrayList<>();
    String line;

    private String readLineFromConsole() {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        return line;
    }

    private void calculateUniqueWords(List<String> wordList) {
        long uniqueWordsAmount = list.stream().distinct().count();
        System.out.println(uniqueWordsAmount); //Number of unique words
        list.stream().
                distinct().
                sorted().
                forEach(s -> System.out.print(s + ", "));
    }

    public void calculateOccurrenceOfWords() {
        Map<String, Long> countedWords = list.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        for (Map.Entry<String, Long> pair : countedWords.entrySet()) {
            System.out.println(pair.getKey() + "  " + pair.getValue());
        }
    }

    public void calculateOccurrenceOfEachSymbol() {
        String[] split = list.toString().split("");

        Map<String, Long> countedLetters = Arrays.asList(split).stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        for (Map.Entry<String, Long> pair : countedLetters.entrySet()) {
            System.out.println(pair.getKey() + " letter with occurrence: " + pair.getValue());
        }
    }

    public void readWords() {
        line = readLineFromConsole();
        while (line != null && !line.isEmpty()) {
            list.add(line);
            line = readLineFromConsole();
        }
        calculateUniqueWords(list);
    }
}
