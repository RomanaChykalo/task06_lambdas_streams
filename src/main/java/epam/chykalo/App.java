package epam.chykalo;

import java.util.*;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        Acceptor averageValue = (a, b, c) -> (a + b + c) / 3;
        Acceptor maxValue = (a, b, c) -> {
            int max1 = Math.max(a, b);
            int max2 = Math.max(c, max1);
            return max2;
        };
        App a = new App();
        a.generateRandomList();
    }

    public void generateRandomList() {
        List<Integer> listOfRandomWords = new ArrayList<>();
        for (int i = 5; i < 30; i++) {
            listOfRandomWords.add(i);
        }
        List result = listOfRandomWords.stream().map(l -> l * 2).limit(5).collect(Collectors.toList());
        System.out.print(result);
        System.out.print(listOfRandomWords.stream().mapToDouble(l -> l).average());
        int sum = result.stream().mapToInt(i -> (int) i).sum();
        OptionalDouble average = result.stream().mapToInt(i -> (int) i).average();
        OptionalInt min = result.stream().mapToInt(i -> (int) i).min();
        OptionalInt max = result.stream().mapToInt(i -> (int) i).max();

        long biggerThenAverageAmount = result.stream().mapToInt(i -> (int) i).filter(i -> i > average.getAsDouble()).count();


    }

}
