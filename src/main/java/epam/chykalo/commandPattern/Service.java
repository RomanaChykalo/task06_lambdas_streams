package epam.chykalo.commandPattern;

public class Service {

    public void drawFace() {
        System.out.println("I will make you a beautiful makeup");
    }

    public void cut() {
        System.out.println("I'll cut your hair");
    }

    public void drawNail() {
        System.out.println("I will make you interesting nail-design");
    }
}

