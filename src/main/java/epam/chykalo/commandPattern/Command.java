package epam.chykalo.commandPattern;

public interface Command {
    void execute();
}

class Hairdresser implements Command {
    private Service service;

    public Hairdresser(Service service) {
        this.service = service;
    }

    public void execute() {
        service.cut();
    }
}

class MakeupArtist implements Command {
    private Service service;

    public MakeupArtist(Service service) {
        this.service = service;
    }

    public void execute() {
        service.drawFace();
    }
}

class Manicurist implements Command {
    private Service service;

    public Manicurist(Service service) {
        this.service = service;
    }
    public void execute() {
        service.drawNail();
    }
}

