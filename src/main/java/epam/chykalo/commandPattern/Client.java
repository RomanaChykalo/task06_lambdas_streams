package epam.chykalo.commandPattern;

import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Service service = new Service();
        System.out.println("Welcome to the Beauty-bar!");
        int selection = 0;
        do{
            System.out.println("[1] make a haircut");
            System.out.println("[2] do make-up");
            System.out.println("[3] make manicure");
            System.out.println("[4] exit");
            Scanner scanner = new Scanner(System.in);
            selection = scanner.nextInt();
            switch (selection)
            {
                case 1:
                    service.cut();
                    break;
                case 2:
                    service.drawFace();
                    break;
                case 3:
                    service.drawNail();
                    break;
                case 4:
                    System.out.println("FINISH");
                default:
                    System.out.println("You enter a wrong command.");

            }
        }
        while(selection!=4);
    }

}
