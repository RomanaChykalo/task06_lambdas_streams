package epam.chykalo;

@FunctionalInterface
public interface Acceptor {
    public int func(int a, int b, int c);

}
